# Templates in LaTeX

This project contains document templates for the thesis (bachelor and diploma) and presentation written in _LaTeX_. Using these templates you can focus more to the content of your work and to writing itself than to the fight with the text processing software such as _MS Word_ or _OO Writer_.

There are two main directories in this project:
* `presentation/` - contains presentation template
* `thesis/` - contains thesis template
* `thesis-manual/` - contains manual for writing thesis

This project is still work in progress. If you will find any issue, don't forget to report it! We are still trying to make it better and easier to use. Our goal is still to provide you the best tools, so you can reach the best results.
